  #include <Wire.h>
#include <SPI.h>
#include <Adafruit_CAP1188.h>

// CAP
#define CAP1188_RESET 9
// CS pin is used for software or hardware SPI
#define CAP1188_CS 53
// These are defined for software SPI
#define CAP1188_MOSI 51
#define CAP1188_MISO 50
#define CAP1188_CLK 52  //SCK
Adafruit_CAP1188 cap = Adafruit_CAP1188(CAP1188_CS, CAP1188_RESET);

/*
  MIDI
  The circuit:
  - digital in 1 connected to MIDI jack pin 5
  - MIDI jack pin 2 connected to ground
  - MIDI jack pin 4 connected to +5V through 220 ohm resistor
  - Attach a MIDI cable to the jack, then to a MIDI synth, and play music.
  CAP
  
*/

// MIDI
const int buttonPin1 = 2;     // the number of the pushbutton pin
int buttonState1 = 0;         // variable for reading the pushbutton status
int note = 0x3E;
int notes[] = {0x3A, 0x4E, 0x5F};
bool played = false;
  
void setup() {
  // Set MIDI baud rate:
  Serial3.begin(31250);
  // initialize the pushbutton pin as an input:
  pinMode(buttonPin1, INPUT);
  
  // CAP
  Serial.begin(9600);
  Serial.println("CAP1188 test!");
  pinMode(49, OUTPUT);
  if (!cap.begin()) {
    Serial.println("CAP1188 not found");
    while (1);
  }
  Serial.println("CAP1188 found!");
}

void loop() {
  // read the state of the pushbutton value:
  buttonState1 = digitalRead(buttonPin1);

   // check if the pushbutton is pressed. If it is, the buttonState is HIGH:
  if (!played && buttonState1 == HIGH) {
     noteOn(notes[0], 0x45);
     played = true;
     
  } else if (played && buttonState1 == LOW) {
    noteOn(notes[0], 0x00);
    played = false;
  } 

  uint8_t touched = cap.touched();

  if (touched == 0) {
    // No touch detected
    return;
  }
  
  for (uint8_t i=0; i<8; i++) {
    if (touched & (1 << i)) {
      if (i + 1 == 6) {
        noteOn(notes[0], 0x45);
        delay(50);
        noteOn(notes[0], 0x00);
       }
      Serial.print("C"); Serial.print(i+1); Serial.print("\t");
    }
  }
  Serial.println();
  delay(50);
}

// plays a MIDI note. Doesn't check to see that cmd is greater than 127, or that
// data values are less than 127:
void noteOn(int pitch, int velocity) {
  Serial3.write(0x90); // command to play the note
  Serial3.write(pitch);
  Serial3.write(velocity);
}
