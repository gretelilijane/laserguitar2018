#include <Wire.h>
#include <SPI.h>
#include <Adafruit_CAP1188.h>

// CAP
#define CAP1188_RESET_3 9
#define CAP1188_RESET_2 8
#define CAP1188_RESET_1 7

// CS pin is used for software or hardware SPI
#define CAP1188_CS_3 49
#define CAP1188_CS_2 48
#define CAP1188_CS_1 47
 
// These are defined for software SPI
#define CAP1188_MOSI 51
#define CAP1188_MISO 50
#define CAP1188_CLK 52  //SCK

/*Adafruit_CAP1188 cap_1 = Adafruit_CAP1188(CAP1188_CS_1, CAP1188_RESET);*/
Adafruit_CAP1188 cap_1 = Adafruit_CAP1188(CAP1188_CS_1, CAP1188_RESET_3);
Adafruit_CAP1188 cap_2 = Adafruit_CAP1188(CAP1188_CS_2, CAP1188_RESET_3);
Adafruit_CAP1188 cap_3 = Adafruit_CAP1188(CAP1188_CS_3, CAP1188_RESET_3);
/*
  MIDI
  The circuit:
  - digital in 1 connected to MIDI jack pin 5
  - MIDI jack pin 2 connected to ground
  - MIDI jack pin 4 connected to +5V through 220 ohm resistor
  - Attach a MIDI cable to the jack, then to a MIDI synth, and play music.
  CAP

  punane - vin
  must - ground
  roheline - miso
  oranz - mosi
  valge - clk

*/

// BUTTON
const int buttonPin = 2;     // the number of the pushbutton pin
int buttonState = 0;         // variable for reading the pushbutton status

// MIDI
int notes[] = {0x3C, 0x3E, 0x40, 0x41, 0x43, 0x45, 0x47, 0x48};

uint8_t touched[] = {0, 0, 0};
uint8_t previousTouch[] = {0, 0, 0};

byte temp = 20;
char myArray[8];
byte data;

void bitPrint(byte temp) {
  Serial.print("0b");
  for (byte i = 0; i < 8; i++)
  {
    Serial.print(bitRead(temp, i)? "1" : "0" );
    myArray[i] = bitRead(temp, i)? "1" : "0";
  }
  Serial.println();
}


void setup() {
  // Set MIDI baud rate:
  Serial3.begin(31250);

  // Button
  pinMode(buttonPin, INPUT);

  // CAP
  Serial.begin(9600);
  Serial.println("CAP1188 test!");
  pinMode(49, OUTPUT);
  pinMode(48, OUTPUT);
  pinMode(47, OUTPUT);
  
  if (!cap_1.begin()) {
    Serial.println("CAP1 not found");
    while (1);
  }else if(!cap_2.begin()) {
    Serial.println("CAP2 not found");
    while (1);
  }else if(!cap_3.begin()) {
    Serial.println("CAP3 not found");
    while (1);
  }
  Serial.println("All CAPs found!");

  // Customize multiple touch register
  data = cap_1.readRegister(0x2A);
  bitPrint(data);
  data = data ^ 0b10000000;    //Current - x2;         Sensitivity mask x4 0b01110000
  cap_1.writeRegister(0x2A, data);
  bitPrint(cap_1.readRegister(0x2A));

  // LED register
  cap_1.writeRegister(0x72, 0xFF);
}

void loop() {

  touched[0] = cap_1.touched();
  touched[1] = cap_2.touched();
  touched[2] = cap_3.touched();
  if (touched[0] != 0 or touched[1] != 0 or touched[2] != 0){
    Serial.println(String(touched[0], BIN) + "; " + String(touched[1], BIN) + "; " + String(touched[2], BIN));
  }

  for (uint8_t i = 0; i < 3; i++) {
    previousTouch[i] = hasNoteChanged(touched[i], previousTouch[i]);
   }
}

uint8_t hasNoteChanged(uint8_t touched, uint8_t previousTouch) {

  for (uint8_t i = 0; i < 8; i++) {
    if (touched ^ previousTouch & (1 << i)) { // Note has changed
      if (touched & (1 << i)) {
        noteOn(notes[i], 0x30);
      } else {
        noteOff(notes[i], 0x15);
      }
    }
  }
  return touched;
}

// plays a MIDI note. Doesn't check to see that cmd is greater than 127, or that
// data values are less than 127:
void noteOn(int pitch, int velocity) {
  //Serial.println("noteON");
  Serial3.write(0x90); // command to play the note
  Serial3.write(pitch);
  Serial3.write(velocity);
}

void noteOff(int pitch, int velocity) {
  //Serial.println("noteON");
  Serial3.write(0x80); // command to stop playing the note
  Serial3.write(pitch);
  Serial3.write(velocity);
}
